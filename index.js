//dependencies setup
require('dotenv').config()
const express = require("express")
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require("./routes/UserRoutes")
const itemRoutes = require("./routes/ItemRoutes")
const orderRoutes = require("./routes/OrderRoutes")

//database connection
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://vindniega:vindeniega@cluster0.isiu6.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', { 
	useNewUrlParser: true, 
	useUnifiedTopology: true 
})

//server setup
const app = express()
const port = 4000
app.use(express.json())
app.use(express.urlencoded({extended:true}))

var corsOptions = {
	origin: 'http://localhost:3000',
	optionsSuccessStatus: 200
  }
  
app.get(cors(corsOptions))

app.use(cors()) //This will be used if other sites would use this API

app.use("/api/users", userRoutes)
app.use("/api/items", itemRoutes)
app.use("/api/orders", orderRoutes)

//server listening
app.listen(process.env.PORT || port, () => console.log(`Listening to port ${port}`))