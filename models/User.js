const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
	//firstname, lastname, email, password, isAdmin, mobileNo, enrollments
	firstname : {
		type : String,
		required : [true, "First name is required"]
	},
	lastname : {
		type : String,
		required : [true, "Last name is required"]
	},
	mobileNo : {
		type : String,
		required : [true, "Mobile Number is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	}, 
	password : {
		type: String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	}
})

module.exports = mongoose.model("User", userSchema)
//Module.exports allows us to this file as a module similar to packages and can be required in other files that need it.