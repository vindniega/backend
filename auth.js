const jwt = require("jsonwebtoken")
const secret = "ecomsample" //secret can be any phrase

//JWT is like a gift wrapping service but with secrets
//Only the person who has the secret can open the gift
//And if the wrapper has been tampered with, JWT also recognizes it and will disregard the gift

//functionality
//createAccessToken -> analog: pack the gift, and sign with the secret
module.exports.createAccessToken = (user) => {
	//the user parameter comes from the login
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	}

	return jwt.sign(data, secret, {})
}

//verify the token -> analog: receive the gift, and verify if sender is legit
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization //we put the JWT in the headers of the request
	//in Postman this is in Authorization -> select "Bearer Token" -> Token

	if(typeof token !== "undefined") {
	//the token exists
		token = token.slice(7, token.length)
		//<string>.slice - cuts the string starting from the value up to the specified value
		//starts at the 7th and ends at the last character
		//Strings are arrays of characters.
		//The first 7 characters are not relevant/related to the actual data we need

		return jwt.verify(token, secret, (err, data) => {
			//jwt.verify verifies the token with the secret and fails if the token's secret doesn't match with the secret phrase (ie the token has been tampered with)
			return (err) ? res.send({auth : "failed"}) : next()
			//next() is a function that allows us to proceed to the next request
		}) 
	} else {
		//if token is empty
		return res.send({auth : "failed"})
	}
}

//decode the token -> analog: open the gift and get the contents
module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			return (err) ? null : jwt.decode(token, {complete:true}).payload
			//jwt.decode -> decodes the token and gets the "payload"
			//payload in this case contain the data from the createAccessToken
		})
	} else {
		return null
	}
}