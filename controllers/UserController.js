const User = require("../models/User") 
const bcrypt = require("bcrypt") 
const auth = require("../auth")

//functions
//check if email already exists
module.exports.emailExists = (parameterFromRoute) => {

	return User.find({ email : parameterFromRoute.email }).then(resultFromFind => {
		return resultFromFind.length > 0 ? true : false 
	})
	//params.email would come from the request when we use the controller in the routes
}


//registration
module.exports.register = (params) => {
	let newUser = new User({
		firstname : params.firstname,
		lastname : params.lastname,
		email : params.email,
		mobileNo : params.mobileNo,
		password : bcrypt.hashSync(params.password, 10)
	})

	return newUser.save().then((user, err) => {
		return (err) ? false : true
	})
}

//login
module.exports.login = (params) => {
	return User.findOne({email : params.email}).then(resultFromFindOne => {
		if (resultFromFindOne == null){ //user doesn't exist
			return false
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, resultFromFindOne.password)

		//at this point, there is a user and the passwords are compared
		if(isPasswordMatched){
			return { access : auth.createAccessToken(resultFromFindOne.toObject())}
		} else {
			return false
		}

	})
}


//get user profile
module.exports.get = (params) => {
	return User.findById(params.userId).then(resultFromFindById => {
		resultFromFindById.password = undefined
		return resultFromFindById
	})
}
