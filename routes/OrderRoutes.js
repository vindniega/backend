const express = require("express")
const route = express.Router() 
const auth = require("../auth")
const OrderController = require("../controllers/OrderController")

route.get("/", (req, res) => {})
route.get("/:itemId", (req, res) => {})
route.post("/", auth.verify, (req, res) => {
	OrderController.createOrder(req.body).then(result => res.send(result))
})
route.put("/:itemId", (req, res) => {})
route.delete("/:itemId", (req, res) => {})

module.exports = route